from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from ..models import NewUser


class NewUserModelTest(TestCase):

    def setUp(self):
        user = get_user_model()
        new_user = user.objects.create_user(email='abc@gmail.com', password='12345')

    def test_create_user(self):
        user = get_user_model()
        new_user = user.objects.create_user(email='vania@gmail.com', password='345')
        self.assertEqual(new_user.email, 'vania@gmail.com')
        self.assertTrue(new_user.is_active)

    def test_delete_user(self):
        NewUser.objects.get().delete()
        with self.assertRaises(Exception):
            NewUser.objects.get()

    def test_create_admin(self):
        user = get_user_model()
        new_user = user.objects.create_superuser(email='abssc@gmail.com', password='123dsa45')
        self.assertEqual(new_user.is_admin, True)
        self.assertEqual(new_user.email, 'abssc@gmail.com')
        self.assertTrue(new_user.is_active)

    def test_authenticate_user(self):
        user = authenticate(email='abc@gmail.com', password='12345')
        user_1 = authenticate(email='vova@gmail.com', password='111345')
        self.assertEqual(user.email, 'abc@gmail.com')
        self.assertEqual(user.id, 1)
        self.assertIsNone(user_1)
